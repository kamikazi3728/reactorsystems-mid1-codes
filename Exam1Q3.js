const coreThermalOutput = 530000; //kWt
const LOCALimit = 9.2; //kW/ft
const bundleArray = 17*17;
const waterRodsPerAssemb = 25;
const BARodsPerAssemb = 18;
const S = 0.496; //inches
const gamma = 0.974;
const Fq = 2.2;
const Lpellet = 0.4; //inches
const coreHoverD = 1.1;

const pi = Math.PI;

//define number of fuel rods per assembly
const fuelRodsPerAssemb = bundleArray-waterRodsPerAssemb-BARodsPerAssemb;console.log("FA/ass",fuelRodsPerAssemb);
const Lassemb = Math.sqrt(bundleArray)*S;console.log("L/ass",Lassemb);
const Aassemb = Lassemb**2;console.log("AAss",Aassemb);

//Solving for Cylindrical core
//solve for NA using eqn from hw3p3
let NA = ((coreThermalOutput*gamma*Fq/(LOCALimit*fuelRodsPerAssemb*coreHoverD*Math.sqrt(4*bundleArray*(S**2)/pi)))**(2/3));console.log(NA);
//Solve for INTEGER N, round up if necessary
let N = Math.ceil((NA-1)/4);
//re-set NA just in case N was rounded
NA = (4*N)+1;
//Solve for number of fuel rods in core
let nRods = NA*fuelRodsPerAssemb;console.log(nRods);
//Solve for numerical value of H based on nRods
let H = coreThermalOutput*gamma*Fq/(LOCALimit*nRods);console.log(H);
//solve for nPellets via H, Lpellet. Round up if necessary
let nPellets = Math.ceil(H/Lpellet);console.log(nPellets);
//Re-solve for H using nPellets just in case it was rounded
H = nPellets*Lpellet;console.log(H);
//Output cylindrical results for part A
console.log("Cylindrical Core:");
console.log("Part A")
console.log("Min # FAs: "+NA);
console.log("Min # fuel rods: "+nRods);
console.log("Fuel height: "+H+" inches, or "+(H/12)+" ft");
console.log("Part B")
//solve for volume
let Vcore = NA*H*Aassemb
//solve buckling
//calculate effective diameter
let Acore = NA*Aassemb;console.log("Acore",Acore);
let Deff = Math.sqrt(4*Acore/pi);console.log("De",Deff);
let reff = Deff/2;console.log("R",reff);
let B = Math.sqrt(((2.405/reff)**2)+((pi/H)**2));
//solve for vessel radius
let rVessel = Lassemb*Math.sqrt(8.5);
//solve for vessel diameter
let dVessel = rVessel*2;
console.log("Core volume: "+Vcore+" in3, or "+(Vcore/(12**3))+" ft3");
console.log("B: "+B+"/in, or "+(B/(12))+"/ft");
console.log("Vessel Diameter: "+dVessel+" in, or "+(dVessel/12)+" ft");
console.log(" ");

//Solving for parallelepiped core
H = ((coreThermalOutput*gamma*Fq*bundleArray*(S**2)/(LOCALimit*fuelRodsPerAssemb))**(1/3));console.log("H1",H,H/12);
N = Math.ceil(H/Lassemb);
NA = N**2;
nRods = NA*fuelRodsPerAssemb;
H = coreThermalOutput*gamma*Fq/(nRods*LOCALimit);console.log("H1",H,H/12);
nPellets = Math.ceil(H/Lpellet);console.log("nP",nPellets);
H = nPellets*Lpellet;console.log("H1",H,H/12);
console.log("Parallelepiped Core:");
console.log("Part A")
console.log("Min # FAs: "+NA);
console.log("Min # fuel rods: "+nRods);
console.log("Fuel height: "+H+" inches, or "+(H/12)+" ft");
console.log("Part B")
//solve for volume
Vcore = NA*H*Aassemb;
//solve buckling
B = Lassemb*(Math.sqrt(NA));
B = (2*((pi/B)**2))+((pi/H)**2);
B = Math.sqrt(B);
//solve for vessel diameter
dVessel = Lassemb*Math.sqrt(2*NA);
console.log("Core volume: "+Vcore+" in3, or "+(Vcore/(12**3))+" ft3");
console.log("B: "+B+"/in, or "+(B/(12))+"/ft");
console.log("Vessel Diameter: "+dVessel+" in, or "+(dVessel/12)+" ft");
console.log(" ");

