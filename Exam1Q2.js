const P = 1045; //psia
const hin = 528; //Btu/lbm
const mCore = 77*(10**6);//lbm/hr
const qavg = 165000; //Btu/hr ft^2
const S = 0.64; //in
const Dfuel = 0.483; //in
const Dwater = 0.591;//in
const H = 150;//in
const Fz = 1.4;
const Acan = 5.278**2;//in^2
const lattice = 8*8;
const NA = 560;
const waterRodsPerAssemb = 2;
const gamma = 0.91;
const voidCoeff = -91;//pcm/%void

const pi = Math.PI;

//Fluid Saturation Properties
let rhof = 45.9519; //lb/ft3
let rhog = 2.35527;//lb/ft3
let hf = 549.498;//Btu/lb
let hg = 1190.85;//Btu/lb
let theta = 0.001177;//lbf/ft (from hw2p2soln)
let hfg = hg-hf;

let nRods=NA*(lattice-waterRodsPerAssemb);console.log("nRods",nRods);
let mCh = mCore/nRods;console.log("mCh",mCh);

let xmax = 2.029; //position of maximum heat flux, BOC (see hw2p3a)

//define cores and qo values
let core = [{},{}];//[BOC,MOC]
core[1].qo = qavg*Fz;
core[0].qo = core[1].qo/(xmax*Math.sin(xmax));

//define extrapolation distance values
core[0].lambda = 3.139*12;//in, from hw2p3app3, H and Fz are the same
core[0].He = (2*core[0].lambda)+H;
console.log("qoMOC",core[1].qo);
console.log("qoBOC",core[0].qo);
//MOC lambda value must be solved for iteratively (like in hw1p1pp3)
function HeTmp(x){return H+(2*x);}
function lambdaFind(x){return ((1/((HeTmp(x)/(pi*H))*(Math.cos(pi*x/HeTmp(x))-Math.cos((pi*(H+x))/HeTmp(x)))))-Fz);}
//find MOC lambda
core[1].lambda=0;
for(var i=0;i<10000;i++){
	let tempz = ((H/10000)*i);
	let toTest = Math.abs(lambdaFind(tempz));
	let toTest2 = Math.abs(lambdaFind(core[1].lambda));
	if(toTest2>toTest){
		core[1].lambda = tempz;
	}
}
console.log("lambda for MOC found to be (inches)",core[1].lambda);
core[1].He = (2*core[1].lambda)+H;
//define fluid enthalpy distributions
core[1].h = function(z){return (hin+((core[1].qo*(Dfuel/12)*(core[1].He/12)/(gamma*mCh))*(Math.cos(pi*core[1].lambda/core[1].He)-Math.cos(pi*(core[1].lambda+z)/core[1].He))));};
core[0].h = function(z){
	let const1 = (pi*(H+core[0].lambda-z))/core[0].He;
	let const2 = (pi*(H+core[0].lambda))/core[0].He;
	return (hin+((core[0].qo*(core[0].He/12)*(Dfuel/12)/(mCh*gamma))*((const1*Math.cos(const1))-(const2*Math.sin(const2))-Math.sin(const1)+Math.sin(const2))));
};

//define function for equilibrium quality
function x(hz){return (hz-hf)/hfg;}

//calculate parameter b for Dix correlation
let b=((rhog/rhof)**0.1); console.log("b: ",b);

//define function for Dix parameter beta
function beta(x){return (x/(x+((1-x)*(rhog/rhof))));}

//function for concentration parameter
function Co(x){
	let betax = beta(x);
	return (betax*(1+(((1/betax)-1)**b)));
}

let gc = 32.17405*60*60;//gravitational const in ft/hr2
let Vgj = 2.9*((theta*gc*gc*(rhof-rhog)/(rhof**2))**(1/4));console.log("Vgj: ",Vgj);


//function to apply Zuber-Findlay for void fraction

let ACore = NA*(Acan-((pi/4)*((waterRodsPerAssemb*(Dwater**2))+((nRods/NA)*(Dfuel**2)))));console.log("Acore: ",ACore);
let G = mCore/ACore;console.log("G: ",G);
function ag(x){return 1/((Co(x)*(1+(((1-x)/x)*(rhog/rhof))))+(rhog*Vgj/(G*x)));}
//integrate over all ag
core[0].a = 0;
core[1].a = 0;
for(var j=0;j<1000;j++){
	let zInd = (H/1000)*j;
	let xz = [x(core[0].h(zInd)),x(core[1].h(zInd))];
	if(xz[0]>0&&xz[0]<=1){
		core[0].a = core[0].a+(ag(xz[0])/1000);
	}
	if(xz[1]>0&&xz[1]<=1){
		core[1].a = core[1].a+(ag(xz[1])/1000);
	}
}
console.log("Average voids: ",core[0].a,core[1].a);
console.log("Void Reactivities (pcm): ",core[0].a*100*voidCoeff,core[1].a*100*voidCoeff);

